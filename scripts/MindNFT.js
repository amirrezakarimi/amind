
const hre = require("hardhat");

async function main() {

  const MindNft = await hre.ethers.getContractFactory("MindNFT");
  const mindnft = await MindNft.deploy();

  await mindnft.deployed();

  console.log("MindNft deployed to:", mindnft.address);
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
