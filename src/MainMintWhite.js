import React from 'react';
import {useState,text} from 'react';
import {ethers, BigNumber} from 'ethers';
import mindNFT from './MindNFT.json';
import mindNFT2 from './MindNFT2.json';

import { Button, Container, Navbar, Nav } from 'react-bootstrap';

import WinText from "./Wintext.json";

import emailjs from '@emailjs/browser';

  //    const mindNFTAddress = "0xf6ed2a7525de78f5d198a27ff5e3850af20eabd8"; //Matic
    // const mindNFTAddress = "0x2ade18035fe50eb2167dc2aacc593c65e89c8607";
  const mindNFTAddress = "0xc4a315930b24aea165309f25830e893feaf08813";

  const MainMintWhite =({accounts, setAccounts}) =>{
  const[mintAmount, setMintAmount] = useState(1);
  const isConnected = Boolean(accounts[0]);
  const ethAddress = accounts[0];
  const show_err = String(null);
  const [errorMessage, setErrorMessage] = useState('');
  const [SuccessMessage, setSuccessMessage] = useState('');
  const mintPrice = 0.02;
  const balance="";





 function sendEmail(hash,wintext){

    var templateParams = {
        hash: hash,
        wintext: wintext
    };

    emailjs.send('mind', 'template_cdpyisd', templateParams ,'RzBxZ7JgvFHKyRi7Z')
        .then(function(response) {
           console.log('SUCCESS!', response.status, response.text);
        }, function(error) {
           console.log('FAILED...', error);
        });
  }

  async function connectAccount(){
    if(window.ethereum){
      const accounts = await window.ethereum.request({
        method: "eth_requestAccounts",
      });
      setAccounts(accounts);
        console.log("accounts", accounts);
    }
  }

  async function handleMint(){
    if (window.ethereum){
      const provider = new ethers.providers.Web3Provider(window.ethereum);
      const signer = provider.getSigner();
      const contract = new ethers.Contract(
        mindNFTAddress,
        mindNFT2.abi,
        signer
      );




      try{
        const response = await contract.mint(BigNumber.from(mintAmount), {
        value:  ethers.utils.parseEther((mintPrice * mintAmount).toString()),
        });
        console.log("response", response);
        // const myArray = err.data.message.split(": ");
        // let word = myArray[1];
       setSuccessMessage(response.hash);




      }catch (err){
        console.log("error", err);
        const myArray = err.data.message.split(": ");
        let word = myArray[1];
        setErrorMessage(word);


      }

    }
  }

  const handleDecrement =() => {
  if (mintAmount <= 1 ) return;
  setErrorMessage(null);
  setSuccessMessage(null);
  setMintAmount(mintAmount - 1);
};
  const handleIncrement =() => {
  if (mintAmount >= 6 ) return;
  setErrorMessage(null);
  setSuccessMessage(null);
  setMintAmount(mintAmount + 1);
};

function clear_ErrorMessage(){
    setErrorMessage('');
  };
function clear_SuccessMessage(){
    setSuccessMessage('');
  };
function win_text(){
  const randomNumber = Math.floor(Math.random() * WinText.length);

  sendEmail(SuccessMessage, WinText[randomNumber] );

  var prefix = '***';
  if(WinText[randomNumber].startsWith(prefix)){
    sendEmail(SuccessMessage, WinText[randomNumber] );
  }

    return(
      <div>{WinText[randomNumber]}</div>
    );
  };




return(
  <Container>




  <div className="content App">
    <h1>CryptoMinds Collection - Whitelist</h1>
    <p>CryptoMinds are 10,000 brains living on the Ethereum blockchain, backed by our P2E puzzle game collection.</p>
    {isConnected ? (

      <div>
          <div className="App1">

          <Button  onClick ={handleDecrement}> - </Button>

          <div >
            <text className="minttext "

                value={mintAmount}
                >{mintAmount}</text>
          </div>

            <Button
              backgroundColor="#693e3"
              borderRadius="5px"
              boxShadow="0px 2px 2px 1px #0F0F0F"
              color="white"
              cursor="pointer"
              fontFamily="inherit"
              padding="10px"
              fontSize="25px"
                onClick ={handleIncrement}> + </Button>

            </div>


          <Button onClick ={handleMint}>{"<-"} Mint Now {"->"}</Button>

      </div>

    ) : (

      <p class="text-danger" >Please connect your MetaMask wallet for mint CryptoMinds.</p>

    )}


    {isConnected ? (
      <div>

        <text >{balance} Your wallet address: {ethAddress.substring(0, 6)}...{ethAddress.substring(ethAddress.length-6, ethAddress.length)}</text>


      </div>
    ) : (

      <div>
      <Button
       onClick={connectAccount}> Connect Wallet </Button>
      </div>

    )}

    {errorMessage &&
      <div class="alert">
        <span class="closebtn" onClick={clear_ErrorMessage} >&times;</span>
         <div class="text-break"> {errorMessage} </div>

     </div>}
      {SuccessMessage &&
        <div class="alert-Success">
          <span class="closebtn" onClick={clear_SuccessMessage} >&times;</span>
          <div class="text-break">  {SuccessMessage} </div>
          <hr></hr>
            {win_text()}
      </div>}

  </div>

  <div className="content">
        <div className="Hinttext">
            Hint: <br></br>
              - Each CryptoMind NFT mint costs {mintPrice} ETH. <br></br>
              - You can see your NFT in your wallet and opensea. <br></br>
            - You can use your NFT in CryptoMind game and win lots of prizes. <br></br>
        </div>
  </div>

</Container>

);

};


export default MainMintWhite;
