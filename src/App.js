import {useState} from 'react';
import './App.css';
import MainMint from './MainMint';
import NavBar from './NavBar';
import Footer from './Footer';
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {

  return (


    <Mint></Mint>

  );
}

export default App;


function Mint() {
  const [accounts, setAccounts] = useState([]);
    return(
      <div class="overlay">
       <NavBar accounts={accounts} setAccounts={setAccounts} />
         <div class="">
           <MainMint  accounts={accounts} setAccounts={setAccounts} />
         </div>
         <div class="App1">
             <Footer/>
         </div>
        <div className="back">
        <div className="moving-background"></div>
        </div>
      </div>
);
}
