import React from 'react';
//import { Box, Flex,  Link ,Spacer } from "@chakra-ui/react"

import Facebook from "./assets/social-media-icons/facebook.png";
import Twitter from "./assets/social-media-icons/twitter.png";
import Email from "./assets/social-media-icons/email.png";
import Discord from "./assets/social-media-icons/discord.png";
import Cryptominds from "./assets/Cryptominds.png";

import {  Container, Navbar, Nav,NavDropdown } from 'react-bootstrap';


const NavBar = ({accounts, setAccounts}) => {
  const isConnected = Boolean(accounts[0]);

  async function connectAccount(){
    if(window.ethereum){
      const accounts = await window.ethereum.request({
        method: "eth_requestAccounts",
      });
      setAccounts(accounts);
    }
  }

  return(

<div>

  <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
  <Container>

   <Navbar.Brand href="https://cryptominds.games/">
    <img alt="" src={Cryptominds} width="30" height="30" className="d-inline-block align-top"/>{' '}CryptoMinds
   </Navbar.Brand>

  <Navbar.Toggle aria-controls="responsive-navbar-nav" />

  <Navbar.Collapse id="responsive-navbar-nav">

    <Nav className="me-auto">
      <Nav.Link href="/">Public-Mint</Nav.Link>
      <Nav.Link href="/Whitelist">Whitelist-Mint</Nav.Link>
      <Nav.Link href="https://testnets.opensea.io/account">Opensea</Nav.Link>
    </Nav>
    <Nav>
      {/*
        <Nav.Link href="#deets"> <img alt="" src={Discord} width="30" height="30" className="d-inline-block align-top"/> </Nav.Link>
        <Nav.Link href="#deets"> <img alt="" src={Twitter} width="30" height="30" className="d-inline-block align-top"/> </Nav.Link>
        <Nav.Link href="#deets"> <img alt="" src={Facebook} width="30" height="30" className="d-inline-block align-top"/> </Nav.Link>
        <Nav.Link href="#deets"> <img alt="" src={Email} width="30" height="30" className="d-inline-block align-top"/> </Nav.Link>
      */}
    </Nav>

  </Navbar.Collapse>
  </Container>
</Navbar>




</div>

//  <div>
//   <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
//     <div class="container-fluid">
//       <button
//         class="navbar-toggler"
//         type="button"
//         data-mdb-toggle="collapse"
//         data-mdb-target="#navbarSupportedContent"
//         aria-controls="navbarSupportedContent"
//         aria-expanded="false"
//         aria-label="Toggle navigation"
//       >
//         <i class="fas fa-bars"></i>
//       </button>
//
//       <div class="collapse navbar-collapse" id="navbarSupportedContent">
//         <a class="navbar-brand mt-2 mt-lg-0" href="#">
//           <img
//             src="https://mdbcdn.b-cdn.net/img/logo/mdb-transaprent-noshadows.webp"
//             height="15"
//             alt="MDB Logo"
//             loading="lazy"
//           />
//         </a>
//         <ul class="navbar-nav me-auto mb-2 mb-lg-0">
//           <li class="nav-item">
//             <a class="nav-link" href="#">Dashboard</a>
//           </li>
//           <li class="nav-item">
//             <a class="nav-link" href="#">Team</a>
//           </li>
//           <li class="nav-item">
//             <a class="nav-link" href="#">Projects</a>
//           </li>
//         </ul>
//       </div>
//
//       <div class="d-flex align-items-center">
//         <a class="text-reset me-3" href="#">
//           <i class="fas fa-shopping-cart"></i>
//         </a>
//
//         <div class="dropdown">
//           <a
//             class="text-reset me-3 dropdown-toggle hidden-arrow"
//             href="#"
//             id="navbarDropdownMenuLink"
//             role="button"
//             data-mdb-toggle="dropdown"
//             aria-expanded="false"
//           >
//             <i class="fas fa-bell"></i>
//             <span class="badge rounded-pill badge-notification bg-danger">1</span>
//           </a>
//           <ul
//             class="dropdown-menu dropdown-menu-end"
//             aria-labelledby="navbarDropdownMenuLink"
//           >
//             <li>
//               <a class="dropdown-item" href="#">Some news</a>
//             </li>
//             <li>
//               <a class="dropdown-item" href="#">Another news</a>
//             </li>
//             <li>
//               <a class="dropdown-item" href="#">Something else here</a>
//             </li>
//           </ul>
//         </div>
//
//         <div class="dropdown">
//           <a
//             class="dropdown-toggle d-flex align-items-center hidden-arrow"
//             href="#"
//             id="navbarDropdownMenuAvatar"
//             role="button"
//             data-mdb-toggle="dropdown"
//             aria-expanded="false"
//           >
//             <img
//               src="https://mdbcdn.b-cdn.net/img/new/avatars/2.webp"
//               class="rounded-circle"
//               height="25"
//               alt="Black and White Portrait of a Man"
//               loading="lazy"
//             />
//           </a>
//           <ul
//             class="dropdown-menu dropdown-menu-end"
//             aria-labelledby="navbarDropdownMenuAvatar"
//           >
//             <li>
//               <a class="dropdown-item" href="#">My profile</a>
//             </li>
//             <li>
//               <a class="dropdown-item" href="#">Settings</a>
//             </li>
//             <li>
//               <a class="dropdown-item" href="#">Logout</a>
//             </li>
//           </ul>
//         </div>
//       </div>
//     </div>
//   </nav>
//
//
// </div>


  //<div className="nav2">
  //   <Flex justify="spacer-between" align="center" padding="2px" >
  //
  // {  /* left Side - Social Media Icons */ }
  //
  //         <Flex justify="spacer-between" width="25%" padding="0 75px" >
  //
  //         </Flex>
  //
  //         <Flex justify="spacer-between" width="25%" padding="0 75px" >
  //           <Link href="https://www.facebook.com">
  //             <Image src={Email} boxSize="42px" margin="0 15px"/>
  //           </Link>
  //         </Flex>
  //
  //         <Flex justify="spacer-between" width="25%" padding="0 75px" >
  //           <Link href="https://www.facebook.com">
  //
  //           </Link>
  //         </Flex>
  //
  //         <Flex justify="spacer-between" width="25%" padding="0 75px" >
  //           <Link href="https://www.facebook.com">
  //             <Image src={Discord} boxSize="42px" margin="0 15px"/>
  //           </Link>
  //         </Flex>
  //
  //
  //
  //     {  /* Right Side - Sections and connectAccount */ }
  //
  //         <Flex
  //         justify="spacer-between"
  //         align="center"
  //         width="40%"
  //         padding="30px" >
  //
  //             <Box margin="0 15px">About</Box>
  //             <Box margin="0 15px">RoadMap</Box>
  //             <Box margin="0 15px">Team</Box>
  //       </Flex>
  //
  //       {  /* Connect
  //
  //       {isConnected ? (
  //         <Box margin="0 15px">Connected</Box>
  //       ) : (
  //         <Button
  //           backgroundColor="#D6517D"
  //           borderRadius="5px"
  //           boxShadow="0px 2px 2px 1px #0F0F0F"
  //           color="white"
  //           cursor="pointer"
  //           fontFamily="inherit"
  //           padding="15px"
  //           margin="0 15px"
  //           fontSize="20px"
  //          onClick={connectAccount}> Connect Wallet </Button>
  //       )}
  //       */ }
  //   </Flex>
  //  </div>
  )
};
export default NavBar;
