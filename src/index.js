
import './index.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import { render } from "react-dom";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import App from "./App";
import WhiteMint from "./WhiteMint";

const rootElement = document.getElementById("root");
render(
  <BrowserRouter>

    <Routes>
      <Route path="" element={<App />} />
        <Route path="Whitelist" element={<WhiteMint />} />
    </Routes>
  </BrowserRouter>,
  rootElement
);
